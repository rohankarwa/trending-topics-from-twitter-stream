package project.integrate;

import java.io.File;

import project.database.DatabaseManager;
import project.jetty.JettyServerManager;
import project.trending.twitter.slotcounter.SlotCounterFactory;

public class MainClass {

	private static DatabaseManager databaseManager;
	
	public static void main(String[] args) {
		int port = Integer.parseInt(args[0]);
		System.out.println("Starting Jetty on Port: " + port);
		JettyServerManager.startJettyServer(port);
		String databaseFileLocation = "." + File.separator + "h2";
		databaseManager = DatabaseManager.getInstance(databaseFileLocation);
		startTwitterTrendingTopicThread();
		//periodicallyPrintTrendingTopics();
	}
/*
	private static void periodicallyPrintTrendingTopics() {
		printTrendingTopics(databaseManager);
		while(true) {
			try {
				Thread.sleep(50000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			printTrendingTopics(databaseManager);
		}
		
	}

	private static void printTrendingTopics(DatabaseManager myObjectDB) {
		List<TopicInformation> trendingTopics = myObjectDB.getTrendingTopics();
		
		System.out.println("-----------------------START--------------------");
		System.out.println("TRENDING TOPICS");
		for(TopicInformation topicInformation : trendingTopics) {
			System.out.println("Topic: "+ topicInformation);
		}
		System.out.println("-----------------------END----------------------");
	}

*/	private static void startTwitterTrendingTopicThread() {
		TwitterTrendingTopicThread twitterTrendingTopicThread = new TwitterTrendingTopicThread();
		Thread thread = new Thread(twitterTrendingTopicThread);
		thread.start();
	}
	
	public static DatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	public static int getTotalSlots() {
		return SlotCounterFactory.TOTAL_SLOTS;
	}

}
