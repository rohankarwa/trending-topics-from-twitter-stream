package project.database.objectdb.dataobjects;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TopicInformation implements Serializable{

	private static final long serialVersionUID = 1L;
	   
	@Id @GeneratedValue
	private long id;
	
	private String topicJsonString;
	private long timeStamp;
	public TopicInformation() {
		
	}
	
	public TopicInformation(long timeStamp, String topicJsonString) {
		this.topicJsonString = topicJsonString;
		this.timeStamp = timeStamp;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}

	public String getTopicJsonString() {
		return topicJsonString;
	}
	
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public void setTopicJsonString(String topicJsonString) {
		this.topicJsonString = topicJsonString;
	}
	
	@Override
	public String toString() {
		return "Timestamp: " + timeStamp + ", Json = " + topicJsonString;
	}
}
