package project.database.objectdb.dataobjects;

import java.util.ArrayList;
import java.util.List;

public class HomogenityScore {

	private List<Long> scores = new ArrayList<Long>();
	
	public void addScore(long score) {
		scores.add(score);
	}
	
	public List<Long> getScores() {
		return scores;
	}
	
	public void reverse() {
		for(int i=0, j = scores.size() - 1; i < j; i++,j--) {
			long temp = scores.get(i);
			scores.set(i, scores.get(j));
			scores.set(j, temp);
		}
	}
}
