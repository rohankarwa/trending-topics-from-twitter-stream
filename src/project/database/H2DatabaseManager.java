package project.database;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import project.database.objectdb.dataobjects.HomogenityScore;
import project.database.objectdb.dataobjects.TopicInformation;

public class H2DatabaseManager extends DatabaseManager {

	private String databaseUrl;
	private final String DELETE_QUERY = "DELETE FROM TOPICS";
	private final String CREATE_TABLE_QUERY = "CREATE TABLE TOPICS (" +
											" time varchar(100), " +
											" topic_summary clob)";
	private final String INSERT_QUERY = "INSERT INTO TOPICS (time, topic_summary) VALUES(?, ?)";
	private final String SELECT_QUERY = "SELECT time, topic_summary FROM TOPICS ORDER BY time DESC LIMIT 20";
	
	private final String CREATE_HOMOGENITY_TABLE_QUERY = "CREATE TABLE HOMOGENITY (" +
											" myid int NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
											" homogenity_score varchar(100))";
	
	private final String INSERT_HOMOGENITY_TABLE_QUERY = "INSERT INTO HOMOGENITY (homogenity_score) " +
															" VALUES(?)";
	
	//144 = 6 slots per hour * 24 hours; Makes homogenity score for last 1 day
	private final String SELECT_HOMOGENITY_TABLE_QUERY = "SELECT homogenity_score " + 
														" FROM HOMOGENITY ORDER BY myid DESC LIMIT 144";
	
	private final String DROP_HOMOGENITY_TABLE = "DROP TABLE HOMOGENITY";
	
	public H2DatabaseManager(String filePath) {
		this.databaseUrl = filePath;
		createTables();
	}

	@Override
	public boolean persistRecord(long time, String jsonMessage) {
		InputStream is = new ByteArrayInputStream(jsonMessage.getBytes());		 
		Reader reader = new BufferedReader(new InputStreamReader(is)); 
		try {
			Connection connection = createConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			preparedStatement.setLong(1, time);
			preparedStatement.setCharacterStream(2, reader);
			preparedStatement.executeUpdate();
			closeConnection(connection);
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteDatabaseEntries() {
		executeQuery(DELETE_QUERY);
		return true;
	}

	@Override
	public List<TopicInformation> getTrendingTopics() {
		List<TopicInformation> listToReturn = new ArrayList<TopicInformation>();
		try {
			Connection connection = createConnection();
			PreparedStatement preparedStament = connection.prepareStatement(SELECT_QUERY);
			ResultSet resultSet = preparedStament.executeQuery();
			while(resultSet.next()) {
				long timeStamp = resultSet.getLong("time");
				Clob clob = resultSet.getClob("topic_summary");
				String json = getStringFromClob(clob);
				listToReturn.add(new TopicInformation(timeStamp, json));
			}
			closeConnection(connection);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return listToReturn;
	}
	
	
	private String getStringFromClob(Clob clob) {
		StringBuffer buffer = new StringBuffer();
		try {
			BufferedReader bufferedReader = new BufferedReader(clob.getCharacterStream());
			String str = "";
			while((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer.toString();
	}

	private void executeQuery(String query) {
		try {
			Connection connection = createConnection();
			PreparedStatement ps = connection.prepareStatement(query);
			ps.execute();
			closeConnection(connection);
		} catch(SQLException e) {
//			if(!query.contains("CREATE"));
//				e.printStackTrace();
		}	
	}

	private Connection createConnection() {
		Connection connection = null;
		try {
			Class.forName("org.h2.Driver");
			String url = "jdbc:h2:" + databaseUrl;
			String user = "rohan";
			String password = "rohan";
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement ps2 = connection.prepareStatement("Show tables;");
			ps2.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return connection;
	}

	private boolean closeConnection(Connection connection) {
		try {
			connection.close();
		} catch(SQLException e) {
			return false;
		}
		return true;
	}
	
	private void createTables() {
		executeQuery(CREATE_TABLE_QUERY);
		executeQuery(DROP_HOMOGENITY_TABLE);
		executeQuery(CREATE_HOMOGENITY_TABLE_QUERY);
	}

	@Override
	public HomogenityScore getHomogenityScoreForLast1Day() {
		HomogenityScore homogenityScore = new HomogenityScore();
		try {
			Connection connection = createConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_HOMOGENITY_TABLE_QUERY);
			ResultSet resultSet = ps.executeQuery();
			while(resultSet.next()) {
				String score = resultSet.getString("homogenity_score");
				homogenityScore.addScore(Long.parseLong(score));
			}
		} catch(SQLException e) {
		}
		return homogenityScore;
	}

	@Override
	public boolean persistHomogenityScore(long score) {
		try {
			Connection connection = createConnection();
			PreparedStatement ps = connection.prepareStatement(INSERT_HOMOGENITY_TABLE_QUERY);
			ps.setString(1, Long.toString(score));
			ps.executeUpdate();
			closeConnection(connection);
		} catch(SQLException e) {
			return false;
		}
		return true;
	}
	
/*	
	public static void main(String args[]) {
		H2DatabaseManager h2DatabaseManager = new H2DatabaseManager("/Users/rohan/tmp/h2");
		h2DatabaseManager.persistRecord(12331, "json1");
		h2DatabaseManager.persistRecord(12331, "json2");
		h2DatabaseManager.persistRecord(12331, "json3");
		h2DatabaseManager.persistRecord(12331, "json4");
		
		System.out.println(h2DatabaseManager.getTrendingTopics());
		//h2DatabaseManager.deleteDatabaseEntries();
		//System.out.println(h2DatabaseManager.getTrendingTopics());
	}
*/
}
