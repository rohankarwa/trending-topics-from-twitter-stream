package project.database;

import java.util.List;

import project.database.objectdb.dataobjects.HomogenityScore;
import project.database.objectdb.dataobjects.TopicInformation;

public abstract class DatabaseManager {
	
	private static DatabaseManager databaseManager = null;
	
	public abstract boolean persistRecord(long time, String jsonMessage);
	public abstract boolean deleteDatabaseEntries();
	public abstract List<TopicInformation> getTrendingTopics();
	public abstract HomogenityScore getHomogenityScoreForLast1Day();
	public abstract boolean persistHomogenityScore(long score);
	
	public static DatabaseManager getInstance(String filePath) {
		if(databaseManager == null) {
			databaseManager = new H2DatabaseManager(filePath);
		}
		return databaseManager;
	}
	
	public static DatabaseManager getInstance() {
		return databaseManager;
	}

}
