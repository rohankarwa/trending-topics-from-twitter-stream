package project.trending.twitter.bolt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import project.trending.twitter.sentiment.SentimentFactory;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/*
 * This is the Bolt which is responsible for filtering out hashtags from the
 * incoming tuple and also generating the sentimental score for the tweet message.
 * 
 * @author Rohan Karwa
 */
public class DataFilterBolt extends BaseBasicBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Map<String, Boolean> hashTagsToSkip = new HashMap<String, Boolean>();
	static {
		hashTagsToSkip.put("#JOB", true);
		hashTagsToSkip.put("#JOBS", true);
		hashTagsToSkip.put("#TWEETMYJOBS", true);
		hashTagsToSkip.put("#OOMF", true);
		hashTagsToSkip.put("#RT", true);
	}
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        String tweet = tuple.getString(0);
        List<String> hashTags = parseHashTag(tweet);
        for(String hashTag : hashTags) {
        	if(hashTagsToSkip.containsKey(hashTag.trim().toUpperCase())) {
        		continue;
        	}
        	double sentimentScore = SentimentFactory.getInstance().getSentimentScore(tweet);
        //	System.out.println("Senti Score : " + sentimentScore + " of Tweet: "+ tweet);
        	collector.emit(new Values(hashTag.trim().toUpperCase(), sentimentScore));
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("hashtag", "sentimentScore"));
    }
    
    private List<String> parseHashTag(String tweetMessage) {
		List<String> tagList = new ArrayList<String>();
		String regex = "#[\\S]+";
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(tweetMessage);
		while(matcher.find()) {
			tagList.add(matcher.group());
		}
		return tagList;
	}

}