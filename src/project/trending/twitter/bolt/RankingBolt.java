package project.trending.twitter.bolt;

import java.util.List;
import java.util.Map;

import project.trending.twitter.rank.MyRankerHashMap;
import project.trending.twitter.rank.Rankable;
import project.trending.twitter.rank.Ranker;
import project.trending.twitter.rank.TwitterRankable;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/*
 * This is Ranking Bolt which ranks the tuple and outputs top k result
 */
public class RankingBolt extends BaseBasicBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Ranker ranker;
	
	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context) {
		//ranker = new MyRanker(10);
		ranker = new MyRankerHashMap(20);
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        if (isTickTuple(tuple)) {
        	//System.out.println("TICKED");
        	List<Rankable> topRankObjects = ranker.getTopKRankObjects();
        	//System.out.println("SIZE: " + topRankObjects.size());
        	for(Rankable rankable : topRankObjects) {
        		TwitterRankable twitterRankObject = (TwitterRankable) rankable;
        		collector.emit(new Values(twitterRankObject.getHashTag(),
        									twitterRankObject.getWeight(),
        									twitterRankObject.getPositiveScore(),
        									twitterRankObject.getNegativeScore(),
        									twitterRankObject.getTupleExtraInformation()));
        		
        		//System.out.println("Emitting Top Result as: " + twitterRankObject);
        	}
        } else {
        	String hashTag = tuple.getString(0);
        	int weight = tuple.getInteger(1);
        	double positiveScore = tuple.getDouble(2);
        	double negativeScore = tuple.getDouble(3);
        	String tupleExtraInformation = tuple.getString(4);
        	TwitterRankable twitterRankable = new TwitterRankable(hashTag, weight, positiveScore, negativeScore, tupleExtraInformation);
        	ranker.addData(twitterRankable);
        }
    }

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("hashTag", "count", "positiveScore", "negativeScore", "extraInformation"));
    }
	
	@Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = BoltConstants.RANKING_BOLT_EMIT_FREQUENCY_IN_SECS;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }

	

    private boolean isTickTuple(Tuple tuple) {
		return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
	            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
	}
}