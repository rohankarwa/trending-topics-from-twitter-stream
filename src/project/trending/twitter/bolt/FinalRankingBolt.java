package project.trending.twitter.bolt;

import java.util.List;
import java.util.Map;

import project.database.DatabaseManager;
import project.trending.twitter.json.JsonConverter;
import project.trending.twitter.rank.MyRankerHashMap;
import project.trending.twitter.rank.Rankable;
import project.trending.twitter.rank.Ranker;
import project.trending.twitter.rank.TwitterRankable;
import project.trending.twitter.utility.MyUtility;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class FinalRankingBolt extends BaseBasicBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Ranker ranker;
	private DatabaseManager databaseManager;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context) {
		//ranker = new MyRanker(10);
		ranker = new MyRankerHashMap(20);
		databaseManager = DatabaseManager.getInstance();
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        if (isTickTuple(tuple)) {
        	long currentTime = MyUtility.getCurrentTime();
        	System.out.println("FINAL_TICKED");
        	List<Rankable> topRankObjects = ranker.getTopKRankObjects();
        	System.out.println("FINAL SIZE: " + topRankObjects.size());
        	
        	long homogenityScore = 0;
        	
        	for(Rankable rankable : topRankObjects) {
        		TwitterRankable twitterRankObject = (TwitterRankable) rankable;
        		databaseManager.persistRecord(currentTime, twitterRankObject.getTupleExtraInformation());
        		homogenityScore = homogenityScore + calculateHomogenity(twitterRankObject.getTupleExtraInformation());
//        		collector.emit(new Values(twitterRankObject.getHashTag(),
//        									twitterRankObject.getWeight(),
//        									twitterRankObject.getPositiveScore(),
//        									twitterRankObject.getNegativeScore(),
//        									twitterRankObject.getTupleExtraInformation()));
        		
        		System.out.println("FINAL: Emitting Top Result as: " + twitterRankObject);
        	}
        	databaseManager.persistHomogenityScore(homogenityScore);
        } else {
        	String hashTag = tuple.getString(0);
        	int weight = tuple.getInteger(1);
        	double positiveScore = tuple.getDouble(2);
        	double negativeScore = tuple.getDouble(3);
        	String tupleExtraInformation = tuple.getString(4);
        	TwitterRankable twitterRankable = new TwitterRankable(hashTag, weight, positiveScore, negativeScore, tupleExtraInformation);
        	ranker.addData(twitterRankable);
        }
    }

	private long calculateHomogenity(String tupleExtraInformation) {
		List<String> weights = JsonConverter.getTupleExtraInformationObjectFromJson(tupleExtraInformation).getWeights();
		if(weights.size() == 0) return 0;
		int latestWeight = Integer.parseInt(weights.get(0));
		return latestWeight * latestWeight;
	}

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("hashTag", "count", "positiveScore", "negativeScore", "extraInformation"));
    }
	
	@Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = BoltConstants.FINAL_RANKING_BOLT_EMIT_FREQUENCY_IN_SECS;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }

	

    private boolean isTickTuple(Tuple tuple) {
		return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
	            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
	}
}