package project.trending.twitter.bolt;

public class BoltConstants {

	public final static int SLIDING_COUNTER_FREQUENCY_IN_SECS = 600;
	public final static int RANKING_BOLT_EMIT_FREQUENCY_IN_SECS = 630;
	public final static int FINAL_RANKING_BOLT_EMIT_FREQUENCY_IN_SECS = 660;
}
