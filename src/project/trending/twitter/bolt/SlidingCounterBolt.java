package project.trending.twitter.bolt;

import java.util.Map;

import project.trending.twitter.json.JsonConverter;
import project.trending.twitter.slotcounter.SlotCounter;
import project.trending.twitter.slotcounter.SlotCounterFactory;
import project.trending.twitter.tuple.MyTuple;
import project.trending.twitter.tuple.MyTupleAggregatedInfo;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * This is the Sliding Counter Bolt. Which keeps the track of the arrival tuple
 * and the number of times its found in stream in Sliding Window Fashion.
 * Whenever Timer Expires, it requests for the aggregated information and slides 
 * and pass on the data to the Rank Bolt for doing the ranking.
 * 
 * @author Rohan Karwa
 *
 */
public class SlidingCounterBolt extends BaseBasicBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SlotCounter slotCounter;
	
	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context) {
		slotCounter = SlotCounterFactory.getSlotCounterInstance();
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        if (isTickTuple(tuple)) {
        //	System.out.println("TICKED");
        	Map<String, MyTupleAggregatedInfo> allTupleInfo = slotCounter.getAllTupleInfoAndSlide();
        	for(String hashTag : allTupleInfo.keySet()) {
        		MyTupleAggregatedInfo aggregateInfo = allTupleInfo.get(hashTag);
        		collector.emit(new Values(hashTag, aggregateInfo.getTotalWeight(),
        							aggregateInfo.getTotalPositiveSentimentScore(),
        							aggregateInfo.getTotalNegativeSentimentScore(),
        							JsonConverter.getJsonString(aggregateInfo.getTupleExtraInformation())));
//        		System.out.println("Emitting: "+ hashTag +" " + aggregateInfo.getTotalWeight() +" " +
//        							aggregateInfo.getTotalPositiveSentimentScore()+" " +
//        							aggregateInfo.getTotalNegativeSentimentScore());
        	}
        } else {
        	String hashTag = tuple.getString(0);
        	double sentimentScore = tuple.getDouble(1);
        	double negativeScore = 0;
        	double positiveScore = 0;
        	if(sentimentScore < 0) {
        		negativeScore = sentimentScore;
        	} else {
        		positiveScore = sentimentScore;
        	}
        	MyTuple myTuple = new MyTuple(hashTag, 1, positiveScore, negativeScore);
        	slotCounter.insertInSlot(myTuple);
        }
    }

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("hashTag", "count", "positiveScore", "negativeScore", "extraInformation"));
    }
	
	@Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = BoltConstants.SLIDING_COUNTER_FREQUENCY_IN_SECS;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }

    private boolean isTickTuple(Tuple tuple) {
		return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
	            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
	}

}