package project.trending.twitter.sentiment;

/**
 * Creating the object of the class that implements Sentiment Interface.
 * @author rohan
 *
 */
public class SentimentFactory {

	private static Sentiment sentiment = new MySentiment();
	
	public synchronized static Sentiment getInstance() {
		if(sentiment == null) {
			sentiment = new MySentiment();
		}
		return sentiment;
	}
}
