package project.trending.twitter.sentiment;

/**
 * Interface exposing the functionality that is supported for the 
 * sentimental analysis.
 * @author Rohan Karwa
 *
 */
public interface Sentiment {
	public double getSentimentScore(String tweetMessage);
}
