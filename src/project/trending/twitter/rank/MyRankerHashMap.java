package project.trending.twitter.rank;

import java.util.ArrayList;
import java.util.List;

public class MyRankerHashMap implements Ranker{

	private List<Rankable> topKRankedObjects; 
	private int k;
	
	public MyRankerHashMap(int k) {
		this.k = k;
		topKRankedObjects = new ArrayList<Rankable>(k);
	}
	
	@Override
	public synchronized void addData(Rankable rankable) {
		if(topKRankedObjects.contains(rankable)) {
			topKRankedObjects.remove(rankable);
			topKRankedObjects.add(rankable);
		} else {
			if(topKRankedObjects.size() < k) {
				topKRankedObjects.add(rankable);
				return;
			}
			Rankable minRanked = getMinRankedObject();
			if(minRanked.compareTo(rankable) < 0 ) {
				topKRankedObjects.remove(minRanked);
				//System.out.println("Removed: " + minRanked + ", Adding: " + rankable);
				topKRankedObjects.add(rankable);
			}
		}
		
	}

	private Rankable getMinRankedObject() {
		Rankable minRanked = topKRankedObjects.get(0);
		for(int i = 1; i < topKRankedObjects.size(); i++) {
			Rankable currentObject = topKRankedObjects.get(i);
			if(currentObject.compareTo(minRanked) < 0) {
				minRanked = currentObject;
			}
		}
		return minRanked;
	}

	@Override
	public List<Rankable> getTopKRankObjects() {
		List<Rankable> listToReturn = new ArrayList<Rankable>();
		listToReturn.addAll(topKRankedObjects);
		return listToReturn;
	}

	@Override
	public void flush() {
		topKRankedObjects = new ArrayList<Rankable>(k);
		
	}

}
