package project.trending.twitter.rank;



/**
 * This class extends Rankable is responsible for implementing compareTo method which would
 * be called during ranking of the objects.
 * If any changes in the way object are to be compared is to be done then this is the place
 * as all the objects in the Ranking Queue would be compared on the basis of the compareTo
 * implementation.
 * @author Rohan Karwa
 *
 */
public class TwitterRankable extends Rankable{

	private String hashTag;
	private int weight;
	private double positiveScore;
	private double negativeScore;
	private String tupleExtraInformation;
	
	public TwitterRankable(String hashTag, int weight, double positiveScore, double negativeScore, 
								String tupleExtraInformation) {
		this.hashTag = hashTag;
		this.weight = weight;
		this.positiveScore = positiveScore;
		this.negativeScore = negativeScore;
		this.tupleExtraInformation = tupleExtraInformation;
	}
	
	@Override
	public int compareTo(Rankable o) {
		TwitterRankable other = (TwitterRankable) o;
		if(this == o) return 0;
		if(other.getWeight() > this.weight) return -1;
		else return 1;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public double getPositiveScore() {
		return positiveScore;
	}
	
	public double getNegativeScore() {
		return negativeScore;
	}
	
	public String getHashTag() {
		return hashTag;
	}
	
	public String getTupleExtraInformation() {
		return tupleExtraInformation;
	}
	
	@Override
	public String toString() {
		return "HashTag: " + hashTag +
				", Weight: " + weight + 
				", PositveScore: " + positiveScore +
				", NegativeScore: " + negativeScore +
				", TupleExtraInformation: " + tupleExtraInformation;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		TwitterRankable other = (TwitterRankable) obj;
		if(this.hashTag.equalsIgnoreCase(other.hashTag)) return true;
		else return false;
	}
}
