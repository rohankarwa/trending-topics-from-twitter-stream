package project.trending.twitter.rank;

import java.util.List;

/**
 * Interface for the Ranking the objects. This act as an API for ranking operations.
 * @author Rohan Karwa
 *
 */
public interface Ranker {

	public void addData(Rankable rankable);
	public List<Rankable> getTopKRankObjects();
	public void flush();
}
