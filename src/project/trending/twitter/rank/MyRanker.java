package project.trending.twitter.rank;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This class is implementation of the Ranker interface and ranks the object.
 * It maintains PriorityQueue (as Heap) for finding top k tuples.
 * @author Rohan Karwa
 *
 */
public class MyRanker implements Ranker{

	private PriorityQueue<Rankable> priorityQueue;
	private int k;
	public MyRanker(int k) {
		this.k = k;
		priorityQueue = new PriorityQueue<Rankable>(k);
	}
	
	@Override
	public void addData(Rankable rankable) {
		if(priorityQueue.size() <k)
			priorityQueue.add(rankable);
		else {
			Rankable rootObject = priorityQueue.peek();
			if(rankable.compareTo(rootObject) > 0) {
				Rankable removed = priorityQueue.remove();
				System.out.println("Removing: " + removed + ".. Adding: " + rankable);
				priorityQueue.add(rankable);
			}
		}
	}

	@Override
	public List<Rankable> getTopKRankObjects() {
		List<Rankable> listToReturn = new ArrayList<Rankable>();
		while(!priorityQueue.isEmpty()) {
			listToReturn.add(priorityQueue.remove());
		}
		flush();
		return listToReturn;
	}

	@Override
	public void flush() {
		priorityQueue = new PriorityQueue<Rankable>(k);
	}
}
