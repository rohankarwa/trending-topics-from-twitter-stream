package project.trending.twitter.rank;

/**
 * This is the abstract class just for the abstraction. Any class which need to 
 * rank its object as per the interface {@link Ranker}, need to extend it. TwitterRankable
 * class extends this class. This class helps to decouple the {@link Ranker} implementation
 * with the objects that are getting ranked and hence making {@link Ranker} implementation 
 * to work on any class that implements Rankable class.
 * 
 * @author Rohan Karwa
 *
 */
public abstract class Rankable implements Comparable<Rankable>{
}
