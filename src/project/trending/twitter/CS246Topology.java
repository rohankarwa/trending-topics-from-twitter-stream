package project.trending.twitter;

import project.trending.twitter.bolt.DataFilterBolt;
import project.trending.twitter.bolt.FinalRankingBolt;
import project.trending.twitter.bolt.RankingBolt;
import project.trending.twitter.bolt.SlidingCounterBolt;
import project.trending.twitter.spout.TwitterSpout;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

/**
 * Entry Class. This class is responsible for setting up the Storm topology 
 * and doing required wiring between the components.
 * 
 * @author Rohan Karwa
 *
 */
public class CS246Topology {

	public static void startTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("spout", new TwitterSpout(), 1);
		
		builder.setBolt("filter", new DataFilterBolt(), 5).shuffleGrouping("spout");
		builder.setBolt("count", new SlidingCounterBolt(), 8).fieldsGrouping("filter", new Fields("hashtag"));
		builder.setBolt("rankPartial", new RankingBolt(), 3).shuffleGrouping("count");
		builder.setBolt("finalrank", new FinalRankingBolt(), 1).shuffleGrouping("rankPartial");		

		Config conf = new Config();
		conf.setDebug(false);
		conf.setMaxTaskParallelism(3);

		LocalCluster cluster = null;
		try {
		cluster = new LocalCluster();
		cluster.submitTopology("word-count", conf, builder.createTopology());
		} catch(Exception e) {
			e.printStackTrace();
		}

//		try {
//			Thread.sleep(600000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}

		//cluster.shutdown();
	}
}
