package project.trending.twitter.slotcounter;

public class SlotCounterFactory {

	public static final int TOTAL_SLOTS = 6;
//	private static SlotCounter slotCounter = new MySlotCounter(TOTAL_SLOTS);
	
	public static SlotCounter getSlotCounterInstance() {
//		if(slotCounter == null) {
//			synchronized (slotCounter) {
//				slotCounter = new MySlotCounter(TOTAL_SLOTS);
//			}
//		}
		return new MySlotCounter(TOTAL_SLOTS);
	}
}
