package project.trending.twitter.slotcounter;

import project.trending.twitter.tuple.MyTuple;
import project.trending.twitter.tuple.MyTupleAggregatedInfo;

/**
 * This is class which provides Circular Link List Implementation.
 * Here ever node corresponds to one slot (pane) of window. All the nodes
 * makes complete window. So, the window (all the nodes of link list) is divided into
 * panes (node in the link list) which keeps the count of occurence and sentiment score
 * of a specific hashTag during the pane time.
 * Whenever a getAggregatedInformationAndRotate is called, this would return the total 
 * information associated with the tuple via looking at all nodes (pan) i.e. complete window
 * and in the end it would clear out content of the oldest node, thus making room for the 
 * new pane.
 * @author Rohan Karwa
 *
 */
public class MyCircularLinkList {

	private String hashTag;
	private int size = 0;
	private Node headNode = null;
	public MyCircularLinkList(String hashTag, int size) {
		this.size = size;
		this.hashTag = hashTag;
		createLinkList();
	}
	
	public void addDataFromTuple(MyTuple mytuple) {
		headNode.addWeight(mytuple.getWeight());
		headNode.addPositiveSentiment(mytuple.getPositiveSentimentScore());
		headNode.addNegativeSentiment(mytuple.getNegativeSentimentScore());
	}
	
	public MyTupleAggregatedInfo getAggregatedInformationAndRotate() {
		MyTupleAggregatedInfo myTupleAggregatedInfo = new MyTupleAggregatedInfo(hashTag);
		Node currentNode = headNode;
		for(int i = 0; i < size ; i++) {
			if(i != 0) {
				currentNode = currentNode.next;
			}
			myTupleAggregatedInfo.addWeight(currentNode.getTotalWeight());
			myTupleAggregatedInfo.addPositiveSentimentScore(currentNode.getTotalPositiveSentimentScore());
			myTupleAggregatedInfo.addNegativeSentimentScore(currentNode.getTotalNegativeSentimentScore());
		}
		//We need to move the slot. Just changing the head node pointer
		headNode = currentNode;
		headNode.reset();
		
		return myTupleAggregatedInfo;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		Node currentNode = headNode;
		for(int i = 0; i< size; i++) {
			buffer.append(currentNode);
			currentNode = currentNode.next;
		}
		return "HashTag: " + hashTag + "LinkList = " + buffer.toString();
	}
	
	private void createLinkList() {
		Node lastNode = null;
		for(int i = 0; i< size; i++) {
			Node currentNode = new Node();
			if(headNode == null) {
				headNode = currentNode;
			} else {
				lastNode.next = currentNode;
			}
			lastNode = currentNode;
		}
		lastNode.next = headNode;
	}
}

class Node {
	Node next = null;
	private int totalWeight = 0;
	private double totalPositiveSentimentScore = 0;
	private double totalNegativeSentimentScore = 0;
	
	public void addWeight(int weight) {
		this.totalWeight += weight;
	}
	
	public void addPositiveSentiment(double score) {
		this.totalPositiveSentimentScore += score;
	}
	
	public void addNegativeSentiment(double score) {
		this.totalNegativeSentimentScore += score;
	}
	
	public int getTotalWeight() {
		return totalWeight;
	}
	
	public double getTotalPositiveSentimentScore() {
		return totalPositiveSentimentScore;
	}
	
	public double getTotalNegativeSentimentScore() {
		return totalNegativeSentimentScore;
	}
	
	public void reset() {
		totalWeight = 0;
		totalPositiveSentimentScore = 0;
		totalNegativeSentimentScore = 0;
	}
	
	@Override
	public String toString() {
		return "Slot-> TotalWeight: " + totalWeight + 
				", TotalPositiveWeight: " + totalPositiveSentimentScore + 
				", TotalNegativeWeight: " + totalNegativeSentimentScore;
	}
}
