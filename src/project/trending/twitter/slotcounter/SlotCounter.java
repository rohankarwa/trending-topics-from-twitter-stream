package project.trending.twitter.slotcounter;

import java.util.Map;

import project.trending.twitter.tuple.MyTuple;
import project.trending.twitter.tuple.MyTupleAggregatedInfo;

/**
 * This interface provides the contract for the SlotCounter. It exposes two methods:
 * 1. insertInSlot: This method inserts the tuple in the latest time slot.
 * 2. getAllTupleInfoAndSlide: This method is responsible for returning the 
 * aggregated information about all the tuples in the complete window, and making a room
 * by clearing oldest slot. After the oldest slot is cleared the new data inserted in
 * the next call to the insertInSlot should go to the cleared slot.
 * @author rohan
 *
 */
public interface SlotCounter {

	public void insertInSlot(MyTuple myTuple);
	public Map<String, MyTupleAggregatedInfo> getAllTupleInfoAndSlide();
}
