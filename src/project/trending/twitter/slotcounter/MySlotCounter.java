package project.trending.twitter.slotcounter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import project.trending.twitter.tuple.MyTuple;
import project.trending.twitter.tuple.MyTupleAggregatedInfo;

/**
 * This class provides the implementation for the SlotCounter Interface.
 * This class maintains "state" for each hashTag in the form of a circular link list.
 * @author Rohan Karwa
 *
 */
public class MySlotCounter implements SlotCounter {

	//Total Slots in the counter
	private int totalSlots;
	
	private Map<String, MyCircularLinkList> hashTagCounter = new HashMap<String, MyCircularLinkList>();
	
	public MySlotCounter(int totalSlot) {
		this.totalSlots = totalSlot;
	}
	
	@Override
	public void insertInSlot(MyTuple mytuple) {
		String hashTag = mytuple.getHashTag();
		MyCircularLinkList slot = hashTagCounter.get(hashTag); 
		if(slot == null) {
			slot = new MyCircularLinkList(hashTag, totalSlots);
			hashTagCounter.put(hashTag, slot);
		}
		slot.addDataFromTuple(mytuple);
	}

	@Override
	public Map<String, MyTupleAggregatedInfo> getAllTupleInfoAndSlide() {
	//	System.out.println(hashTagCounter);
		Map<String, MyTupleAggregatedInfo> mapToReturn = new HashMap<String, MyTupleAggregatedInfo>();
		Iterator<Entry<String, MyCircularLinkList>> it = hashTagCounter.entrySet().iterator();
		while (it.hasNext()) {
			 Entry<String, MyCircularLinkList> item = it.next();
			 MyTupleAggregatedInfo aggregatedInfo = item.getValue().getAggregatedInformationAndRotate();
			 if(aggregatedInfo.getTotalWeight() == 0) {
				 it.remove();
				 continue;
			 } else {
				 mapToReturn.put(item.getKey(), aggregatedInfo);
			 }
		}
	/*	for(String hashTag : hashTagCounter.keySet()) {
			MyTupleAggregatedInfo aggregatedInfo = hashTagCounter.get(hashTag).getAggregatedInformationAndRotate();
			if(aggregatedInfo.getTotalWeight() == 0) {
				hashTagCounter.remove(hashTag);
				continue;
			} else {
				mapToReturn.put(hashTag, aggregatedInfo);
			}
		}
	*/
		return mapToReturn;
	}
}
