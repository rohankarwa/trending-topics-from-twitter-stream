package project.trending.twitter.spout;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class TwitterSpout extends BaseRichSpout{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SpoutOutputCollector _collector;
	LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>(500);
	
	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
		checkStreamAPI();
		
	}

	@Override
	public void nextTuple() {
		//System.out.println("Queue Size: " + queue.size());
		String nextValue = "";
		nextValue = queue.poll();
		if(nextValue != null) 
			_collector.emit(new Values(nextValue));
		else
			Utils.sleep(10);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
		
	}

	private void checkStreamAPI() {
		try {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			 cb.setDebugEnabled(true)
			 .setOAuthConsumerKey("e4lWezRXPyrNm8WSVk3JwQ")
			 .setOAuthConsumerSecret("iUZpgoSgbeFHFWkPpkkK3pJq1ib2GvLsI29pq5BBgo")
			 .setOAuthAccessToken("1355875069-TwHQCdRkTt8a9urzVxNcAlHZNIiOtaE0JsF8Fqu")
			 .setOAuthAccessTokenSecret("ajaeuQZowiIgWYOHE8MrXtsDELcUfxRT7TUiU7na5o");

			 TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		        StatusListener listener = new StatusListener() {
		            @Override
		            public void onStatus(Status status) {
		              //  System.out.println(status.getText());
		                try {
		                	queue.put(status.getText());
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
		            }

		            @Override
		            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		              //  System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
		            }

		            @Override
		            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
		                //System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
		            }

		            @Override
		            public void onScrubGeo(long userId, long upToStatusId) {
		                //System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
		            }

		            @Override
		            public void onStallWarning(StallWarning warning) {
		                System.out.println("Got stall warning:" + warning);
		            }

		            @Override
		            public void onException(Exception ex) {
		                ex.printStackTrace();
		            }
		        };
		        twitterStream.addListener(listener);
		      //  twitterStream.sample();	 
		        FilterQuery fq= new FilterQuery();
		        //double[][] locations = { {-118.584137, 33.619194}, {-117.996368,  34.211802} };
		        double[][] locations = { {-125.888675, 26.588527}, {-59.970707, 48.69096} };
//		        33.619194,-118.584137
//		        34.211802,-117.996368
//		        double[][] locations = new double[1][2];
//		        locations[0][0] = -74.40;
//		        locations[0][1] = -73.41;
		        fq.locations(locations);
		        twitterStream.filter(fq);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

/*	private List<String> getTags(String tweetMessage) {
		List<String> tagList = new ArrayList<String>();
		String regex = "#[\\S]+";
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(tweetMessage);
		while(matcher.find()) {
			tagList.add(matcher.group());
		}
		return tagList;
	}
*/
}
