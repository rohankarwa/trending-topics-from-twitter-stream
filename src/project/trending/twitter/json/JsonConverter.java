package project.trending.twitter.json;

import com.google.gson.Gson;

public class JsonConverter {

	public static String getJsonString(TupleExtraInformation tupleExtraInformation) {
		Gson gson = new Gson();
		return gson.toJson(tupleExtraInformation);
	}
	
	public static TupleExtraInformation getTupleExtraInformationObjectFromJson(String json) {
		Gson gson = new Gson();
		TupleExtraInformation tupleExtraInformation = gson.fromJson(json, TupleExtraInformation.class);
		return tupleExtraInformation;
	}
	
	public static String getJsonString(AllTopicInformation allTopicInformation) {
		Gson gson = new Gson();
		return gson.toJson(allTopicInformation);
	}
}
