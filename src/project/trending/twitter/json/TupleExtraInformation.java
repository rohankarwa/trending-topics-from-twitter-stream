package project.trending.twitter.json;

import java.util.ArrayList;
import java.util.List;

public class TupleExtraInformation implements Comparable<TupleExtraInformation>{
	private String hashTag;
	private List<String> weightsInDescendingChronologicalOrder = new ArrayList<String>();
	private List<String> positiveScoreInDescendingChronologicalOrder = new ArrayList<String>();
	private List<String> negativeScoreInDescendingChronologicalOrder = new ArrayList<String>();
	
	public TupleExtraInformation(String hashTag) {
		this.hashTag = hashTag;
	}
	
	public String getHashTag() {
		return hashTag;
	}
	
	public List<String> getWeightsInDescendingChronologicalOrder() {
		return weightsInDescendingChronologicalOrder;
	}
	
	public List<String> getPositiveScoreInDescendingChronologicalOrder() {
		return positiveScoreInDescendingChronologicalOrder;
	}
	
	public List<String> getNegativeScoreInDescendingChronologicalOrder() {
		return negativeScoreInDescendingChronologicalOrder;
	}
	
	public void addWeight(String weight) {
		weightsInDescendingChronologicalOrder.add(weight);
	}
	
	public void addPositiveScore(String positiveScore) {
		positiveScoreInDescendingChronologicalOrder.add(positiveScore);
	}
	
	public void addNegativeScore(String negaticeScore) {
		negativeScoreInDescendingChronologicalOrder.add(negaticeScore);
	}

	public void formatInformationForUI() {
		makeAscending(weightsInDescendingChronologicalOrder);
		makeAscending(positiveScoreInDescendingChronologicalOrder);
		makeAscending(negativeScoreInDescendingChronologicalOrder);

		for(int i=0; i< negativeScoreInDescendingChronologicalOrder.size(); i++) {
			double currentValue = Double.parseDouble(negativeScoreInDescendingChronologicalOrder.get(i));
			negativeScoreInDescendingChronologicalOrder.set(i, Double.toString(currentValue * -1));
		}
	}

	private void makeAscending(List<String> descendingOrderWeights) {
		for(int i = 0, j = descendingOrderWeights.size() - 1; i < j; i++, j--) {
			String temp = descendingOrderWeights.get(i);
			descendingOrderWeights.set(i, descendingOrderWeights.get(j));
			descendingOrderWeights.set(j, temp);
		}		
	}

	@Override
	public int compareTo(TupleExtraInformation otherObject) {
		int thisObjectWeight = getTotalWeight(weightsInDescendingChronologicalOrder);
		int otherObjectWeight = getTotalWeight(otherObject.getWeightsInDescendingChronologicalOrder());
		if(thisObjectWeight == otherObjectWeight) return 0;
		if(thisObjectWeight > otherObjectWeight) return -1;
		else return 1;
	}

	private int getTotalWeight(List<String> weightOrder) {
		int count = 0;
		for(String weight : weightOrder) {
			count = count + Integer.parseInt(weight);
		}
		return count;
	}
	
	public List<String> getWeights() {
		return weightsInDescendingChronologicalOrder;
	}
	
	@Override
	public String toString() {
		return "Topic: " + hashTag;
	}
	
	public void setHashTag(String value) {
		hashTag = value;
	}
}
