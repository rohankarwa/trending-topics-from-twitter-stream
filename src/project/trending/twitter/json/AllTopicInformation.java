package project.trending.twitter.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AllTopicInformation {

	private List<TupleExtraInformation> topicList = new ArrayList<TupleExtraInformation>();
	
	public void addTopic(TupleExtraInformation tupleExtraInformation) {
		topicList.add(tupleExtraInformation);
	}
	
	public List<TupleExtraInformation> getTopicList() {
		return topicList;
	}

	public void sortByWeight() {
		Collections.sort(topicList);
	}
	
	@Override
	public String toString() {
		return "Topic List"  + topicList;
	}
}
