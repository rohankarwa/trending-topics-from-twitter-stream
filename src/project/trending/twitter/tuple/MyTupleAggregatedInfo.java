package project.trending.twitter.tuple;

import project.trending.twitter.json.TupleExtraInformation;

public class MyTupleAggregatedInfo {

	private String hashTag;
	private int totalWeight = 0 ;
	private double totalPositiveSentimentScore = 0;
	private double totalNegativeSentimentScore = 0;
	private TupleExtraInformation tupleExtraInformation;
	
	public MyTupleAggregatedInfo(String hashTag) {
		this.hashTag = hashTag;
		tupleExtraInformation = new TupleExtraInformation(hashTag);
	}
	
	public void addWeight(int weight) {
		tupleExtraInformation.addWeight(Integer.toString(weight));
		totalWeight = totalWeight + weight;
	}
	
	public void addPositiveSentimentScore(double score) {
		tupleExtraInformation.addPositiveScore(Double.toString(score));
		totalPositiveSentimentScore = totalPositiveSentimentScore + score;
	}
	
	public void addNegativeSentimentScore(double score) {
		tupleExtraInformation.addNegativeScore(Double.toString(score));
		totalNegativeSentimentScore = totalNegativeSentimentScore + score;
	}
	
	public String getHashTag() {
		return hashTag;
	}

	public int getTotalWeight() {
		return totalWeight;
	}

	public double getTotalPositiveSentimentScore() {
		return totalPositiveSentimentScore;
	}
	
	public double getTotalNegativeSentimentScore() {
		return totalNegativeSentimentScore;
	}	
	
	public TupleExtraInformation getTupleExtraInformation() {
		return tupleExtraInformation;
	}
	
	public void setTupleExtraInformation(TupleExtraInformation tupleExtraInformation) {
		this.tupleExtraInformation = tupleExtraInformation;
	}
}
