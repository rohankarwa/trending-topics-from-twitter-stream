package project.trending.twitter.tuple;

/**
 * This is the Tuple class which holds the information about the tuple.
 * SlotCounter takes this class as input and persist the related information 
 * in the slots.
 * @author rohan
 *
 */
public class MyTuple {

	private String hashTag;
	private int weight;
	private double positiveSentimentScore;
	private double negativeSentimentScore;
	
	public MyTuple(String hashTag, int weight, double positiveSentimentScore, double neagativeSentinmentScore) {
		this.hashTag = hashTag;
		this.weight = weight;
		this.positiveSentimentScore = positiveSentimentScore;
		this.negativeSentimentScore = neagativeSentinmentScore;
	}
	
	public String getHashTag() {
		return hashTag;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public double getPositiveSentimentScore() {
		return positiveSentimentScore;
	}
	
	public double getNegativeSentimentScore() {
		return negativeSentimentScore;
	}
}
