package project.ranker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import project.trending.twitter.json.AllTopicInformation;
import project.trending.twitter.json.TupleExtraInformation;

public class ZScoreTopicRanker extends TopicRanker {

	private int totalSlots = 0;
	private int halfSlots = 0;
	private TopicProperties globalTopicProperty;
	private Map<TupleExtraInformation, TopicProperties> topicInfoPropertiesMap = new HashMap<TupleExtraInformation, TopicProperties>();
	private Map<TupleExtraInformation, TopicZ> topicZMap = new HashMap<TupleExtraInformation, TopicZ>();
	public ZScoreTopicRanker(int totalSlots) {
		this.totalSlots = totalSlots;
		halfSlots = totalSlots/2;
	}


	@Override
	public AllTopicInformation rankTopics(AllTopicInformation allTopicInformation) {
		populateTopicPropertiesMap(allTopicInformation);
		globalTopicProperty = createGlobalProperty(allTopicInformation);
		createTopicZMap(allTopicInformation);
		
		//System.out.println("Topic Z Map: " + topicZMap);
		
		AllTopicInformation topicInfoToReturn = new AllTopicInformation();
		List<TupleExtraInformation> finalRankList = topicInfoToReturn.getTopicList();
		
		List<TupleExtraInformation> allPositiveLocalAndGlobalZ = getAllPositiveLocalAndGlobalZTopics();
		finalRankList.addAll(allPositiveLocalAndGlobalZ);
		
		List<TupleExtraInformation> onePositiveZTopics = getOnePositiveZTopics();
		finalRankList.addAll(onePositiveZTopics);
		
		List<TupleExtraInformation> bothNegativeZTopics = getBothNegativeZTopics();
		finalRankList.addAll(bothNegativeZTopics);
		
		return topicInfoToReturn;
	}
	
	private List<TupleExtraInformation> getOnePositiveZTopics() {
		List<TupleExtraInformation> onePositive = new ArrayList<TupleExtraInformation>();
		for(TupleExtraInformation topic : topicZMap.keySet()) {
			TopicZ topicZ = topicZMap.get(topic);
			if((topicZ.getMeanZWithGlobalParameters() < 0 && 
					topicZ.getMeanZWithLocalParameters() > 0) ||
				(topicZ.getMeanZWithGlobalParameters() > 0 && 
						topicZ.getMeanZWithLocalParameters() < 0)){
				onePositive.add(topic);
			}
		}
		sortOnZ(onePositive, true);
		return onePositive;
	}


	private List<TupleExtraInformation> getBothNegativeZTopics() {
		List<TupleExtraInformation> allNegative = new ArrayList<TupleExtraInformation>();
		for(TupleExtraInformation topic : topicZMap.keySet()) {
			TopicZ topicZ = topicZMap.get(topic);
			
			//DELETE IT
			//topic.setHashTag(topic.getHashTag() + ":" + topicZ);
			
			if(topicZ.getMeanZWithGlobalParameters() < 0 && 
					topicZ.getMeanZWithLocalParameters() < 0) {
				allNegative.add(topic);
			}
		}
		sortOnZ(allNegative, false);
		return allNegative;
	}


	private List<TupleExtraInformation> getAllPositiveLocalAndGlobalZTopics() {
		List<TupleExtraInformation> allPositive = new ArrayList<TupleExtraInformation>();
		for(TupleExtraInformation topic : topicZMap.keySet()) {
			TopicZ topicZ = topicZMap.get(topic);
			if(topicZ.getMeanZWithGlobalParameters() >= 0 && 
					topicZ.getMeanZWithLocalParameters() >= 0) {
				allPositive.add(topic);
			}
		}
		sortOnZ(allPositive, false);
		return allPositive;
	}
	
	private void sortOnZ(List<TupleExtraInformation> topics, boolean sortOnLocal) {
		for(int i = 0; i < topics.size(); i++) {
			for(int j = i + 1; j < topics.size(); j++) {
				double valueI = 0;
				double valueJ = 0;
				if(sortOnLocal) {
					valueI = topicZMap.get(topics.get(i)).getMeanZWithLocalParameters();
					valueJ = topicZMap.get(topics.get(j)).getMeanZWithLocalParameters();
				} else {
					valueI = topicZMap.get(topics.get(i)).getMeanZWithGlobalParameters();
					valueJ = topicZMap.get(topics.get(j)).getMeanZWithGlobalParameters();
				}
				if(valueJ > valueI) {
					TupleExtraInformation temp = topics.get(i);
					topics.set(i, topics.get(j));
					topics.set(j, temp);
				}
			}
		}
	}


	private void createTopicZMap(AllTopicInformation allTopicInformation) {
		for(TupleExtraInformation tupleExtraInformation : allTopicInformation.getTopicList()) {
			double totalGlobalZ = 0;
			double totalLocalZ = 0;
			int count = 0;
			TopicProperties topicProperties = topicInfoPropertiesMap.get(tupleExtraInformation);
			for(int i = halfSlots; i < totalSlots; i++) {
				totalLocalZ = totalLocalZ + getZ(tupleExtraInformation.getWeights().get(i),
												topicProperties.getMean(),
												topicProperties.getStandardDeviation());
				totalGlobalZ = totalGlobalZ + getZ(tupleExtraInformation.getWeights().get(i),
												globalTopicProperty.getMean(),
												globalTopicProperty.getStandardDeviation());
				count = count + 1;
			}
			TopicZ topicZ = new TopicZ(totalGlobalZ/count, totalLocalZ/count);
			topicZMap.put(tupleExtraInformation, topicZ);
		}
	}

	private double getZ(String value, double mean, double deviation) {
		return getZ(Double.parseDouble(value), mean, deviation);
	}
	
	private double getZ(double value, double mean, double deviation) {
		if(deviation == 0) return 0;
		return (value - mean)/deviation;
	}
	
	private TopicProperties createGlobalProperty(AllTopicInformation allTopicInformation) {
		List<String> halfWindowWeightsForAllTopics = new ArrayList<String>();
		for(TupleExtraInformation tupleExtraInformation : allTopicInformation.getTopicList()) {
			for(int i = 0; i< halfSlots; i++) {
				halfWindowWeightsForAllTopics.add(tupleExtraInformation.getWeights().get(i));
			}
		}
		return new TopicProperties(halfWindowWeightsForAllTopics);
	}


	private void populateTopicPropertiesMap(AllTopicInformation allTopicInformation) {
		for(TupleExtraInformation tupleExtraInformation : allTopicInformation.getTopicList()) {
			TopicProperties topicProperties = getTopicProperties(tupleExtraInformation);
			topicInfoPropertiesMap.put(tupleExtraInformation, topicProperties);
		}
	}


	private TopicProperties getTopicProperties(TupleExtraInformation tupleExtraInformation) {
		List<String> halfWindowWeights = new ArrayList<String>();
		for(int i = 0; i < halfSlots; i++) {
			halfWindowWeights.add(tupleExtraInformation.getWeights().get(i));
		}
		TopicProperties topicProperties = new TopicProperties(halfWindowWeights);
		return topicProperties;
	}

}

class TopicProperties {
	private double mean = 0;
	private double standardDeviation = 0;
	
	public TopicProperties(List<String> weights) {
		List<Double> newWeights = stringToDouble(weights);
		mean = calculateMean(newWeights);
		standardDeviation = calculateStandardDeviation(newWeights);
	}
	
	private double calculateStandardDeviation(List<Double> newWeights) {
		double square = 0;
		int count = 0;
		for(Double weight : newWeights) {
			square = square + Math.pow((weight - mean), 2);
			count = count + 1;
		}
		if(count == 0 ) return 0;
		return Math.sqrt(square/count);
	}

	private double calculateMean(List<Double> newWeights) {
		int count = 0;
		double sum = 0;
		for(Double weight : newWeights) {
			count = count + 1;
			sum = sum + weight;
		}
		if(count == 0 ) return 0;
		return sum/count;
	}

	private List<Double> stringToDouble(List<String> weights) {
		List<Double> listToReturn = new ArrayList<Double>();
		for(String weight : weights) {
			listToReturn.add(Double.parseDouble(weight));
		}
		return listToReturn;
	}

	public double getMean() {
		return mean;
	}
	
	public double getStandardDeviation() {
		return standardDeviation;
	}
}

class TopicZ {
	private double meanZWithGlobalParameters;
	private double meanZWithLocalParameters;
	public TopicZ(double meanZWithGlobalParameters, double meanZWithLocalParameters) {
		this.meanZWithGlobalParameters = meanZWithGlobalParameters;
		this.meanZWithLocalParameters = meanZWithLocalParameters;
	}
	
	public double getMeanZWithGlobalParameters() {
		return meanZWithGlobalParameters;
	}
	
	public double getMeanZWithLocalParameters() {
		return meanZWithLocalParameters;
	}	
	
	@Override
	public String toString() {
		return "LocalZ = " + meanZWithLocalParameters + 
				"\tGlobalZ = " + meanZWithGlobalParameters;
	}
}
