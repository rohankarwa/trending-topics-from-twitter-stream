package project.ranker;

import project.trending.twitter.json.AllTopicInformation;

public abstract class TopicRanker {
	

	
	public static TopicRanker getInstance(int totalSlots) {
		return new ZScoreTopicRanker(totalSlots);
	}

	public abstract AllTopicInformation rankTopics(AllTopicInformation allTopicInformation);
}
