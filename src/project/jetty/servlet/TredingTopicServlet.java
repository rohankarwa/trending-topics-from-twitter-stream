package project.jetty.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project.database.DatabaseManager;
import project.database.objectdb.dataobjects.TopicInformation;
import project.integrate.MainClass;
import project.ranker.TopicRanker;
import project.trending.twitter.json.AllTopicInformation;
import project.trending.twitter.json.JsonConverter;
import project.trending.twitter.json.TupleExtraInformation;

public class TredingTopicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DatabaseManager databaseManager = MainClass.getDatabaseManager();
		List<TopicInformation> topicInfoList = databaseManager.getTrendingTopics();
		List<TupleExtraInformation> tupleExtraInformationList = converToTupleExtraInformationList(topicInfoList);
		AllTopicInformation allTopicInformation = getAllTopicInformation(tupleExtraInformationList);
		
		//allTopicInformation.sortByWeight();
		//System.out.println("BEFORE-Ranker: " + allTopicInformation);
		allTopicInformation = TopicRanker.getInstance(MainClass.getTotalSlots()).rankTopics(allTopicInformation);
		
		//System.out.println("AFTER-RANKER: " + allTopicInformation);
		
		String jsonString = JsonConverter.getJsonString(allTopicInformation);
		
		response.getWriter().write(jsonString);
	}
	
	private List<TupleExtraInformation> converToTupleExtraInformationList(List<TopicInformation> topicInfo) {
		List<TupleExtraInformation> listToReturn = new ArrayList<TupleExtraInformation>();
		for(TopicInformation topicInformation : topicInfo) {
			TupleExtraInformation tupleExtraInformation = JsonConverter.getTupleExtraInformationObjectFromJson(topicInformation.getTopicJsonString());
			tupleExtraInformation.formatInformationForUI();
			listToReturn.add(tupleExtraInformation);
		}
		return listToReturn;
	}
	
	private AllTopicInformation getAllTopicInformation(List<TupleExtraInformation> tupleExtraInfoList) {
		AllTopicInformation allTopicInformation = new AllTopicInformation();
		for(TupleExtraInformation tupleExtraInformation : tupleExtraInfoList) {
			allTopicInformation.addTopic(tupleExtraInformation);
		}
		return allTopicInformation;
	}
}
