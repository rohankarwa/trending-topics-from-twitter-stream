package project.jetty.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import project.database.objectdb.dataobjects.HomogenityScore;
import project.integrate.MainClass;


public class HomogenityScoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HomogenityScore homogenityScore = MainClass.getDatabaseManager().getHomogenityScoreForLast1Day();
		homogenityScore.reverse();
		String jsonString = new Gson().toJson(homogenityScore);
		response.getWriter().write(jsonString);
	}
}