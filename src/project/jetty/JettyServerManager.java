package project.jetty;


/**
 * This class handles all the interactions with the Jetty Server
 * 
 * @author rohan
 * 
 */
public class JettyServerManager {

	private static Thread jettyThread;

	public static boolean startJettyServer(int adminPort) {
		JettyServer jettyServer = new JettyServer(adminPort);
		jettyThread = new Thread(jettyServer);
		jettyThread.start();
		return true;
	}

	public static boolean stopServer() {
		jettyThread.interrupt();
		return true;
	}

	public static boolean restartServer(int adminPort, int httpsPort) {
		JettyServer.stopServer();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		startJettyServer(adminPort);//, httpsPort);

		return true;
	}
}
