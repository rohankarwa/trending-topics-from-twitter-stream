package project.jetty;

import java.io.File;




import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;

public class JettyServer implements Runnable{

	private int adminPort;
	private static Server server;
	
	
	public JettyServer(int adminPort) {
		this.adminPort = adminPort;
	}
	
	@Override
	public void run() {
		 WebAppContext context = getContext();
		 server = new Server();
		 if(adminPort != 0) {
			 server.addConnector(getLocalHostConnector(adminPort));
		 }
		 server.setHandler(context);
	     try {
			server.start();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	    try {
			server.join();
		} catch (InterruptedException e) {
			try {
				server.stop();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public static void stopServer() {
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Connector getLocalHostConnector(int portNumber) {
		SelectChannelConnector connector1 = new SelectChannelConnector();
        connector1.setPort(portNumber);
        connector1.setThreadPool(new QueuedThreadPool(20));
        connector1.setName("admin");
        return connector1;
	}

	private WebAppContext getContext() {
		WebAppContext context = new WebAppContext();
		
		context = loadClassesInJetty(context);
		
		File file = new File(System.getProperty("java.io.tmpdir"));
		//TODO
		//File file = new File("/Users/rohan/tmp");
		context.setTempDirectory(file);
		
		
		context.setContextPath("/");
		return context;
	}

	private WebAppContext loadClassesInJetty(WebAppContext context) {
		String webDir = "." + File.separator + "webapp";
		context.setParentLoaderPriority(true);
		context.setResourceBase(webDir);
		context.setDescriptor(webDir+File.separator+"WEB-INF"+File.separator+"web.xml");	
		return context;
	}
}
